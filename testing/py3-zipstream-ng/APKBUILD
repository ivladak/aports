# Contributor: Lauren N. Liberda <lauren@selfisekai.rocks>
# Maintainer: Lauren N. Liberda <lauren@selfisekai.rocks>
pkgname=py3-zipstream-ng
pkgver=1.4.0
pkgrel=0
pkgdesc="A modern and easy to use streamable zip file generator"
url="https://github.com/pR0Ps/zipstream-ng"
arch="noarch"
license="LGPL-3.0-only"
depends="python3"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="py3-pytest"
source="https://github.com/pR0Ps/zipstream-ng/archive/refs/tags/v$pkgver/zipstream-ng-$pkgver.tar.gz"
builddir="$srcdir/zipstream-ng-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
db5ab6aca915ba9c217f31b691f8d385873219ab998eefe4a6661b37d7ae97502d652046fdc8ff9c31321423afad0c7fa38f39fa361d8ec4ced684031c03d49f  zipstream-ng-1.4.0.tar.gz
"
