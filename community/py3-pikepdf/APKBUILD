# Maintainer: Carlo Landmeter <clandmeter@alpinelinux.org>
pkgname=py3-pikepdf
_pyname=pikepdf
pkgver=7.1.2
pkgrel=0
pkgdesc="Python library for reading and writing PDF"
url="https://github.com/pikepdf/pikepdf"
arch="all"
license="MPL-2.0"
depends="
	py3-deprecation
	py3-lxml
	py3-packaging
	py3-pillow
	python3
	"
makedepends="
	py3-gpep517
	py3-installer
	py3-pybind11-dev
	py3-setuptools
	py3-setuptools_scm
	py3-wheel
	python3-dev
	qpdf-dev
	"
checkdepends="
	py3-hypothesis
	py3-psutil
	py3-pytest
	py3-pytest-xdist
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/pikepdf/pikepdf/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"

export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver

# secfixes:
#   2.9.1-r2:
#     - CVE-2021-29421

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python -m installer -d test_install \
		dist/pikepdf-*.whl
	PYTHONPATH="$(echo $PWD/test_install/usr/lib/python3*/site-packages)" pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/pikepdf-*.whl
}

sha512sums="
954ee66f2b1efcdc2f52b4361a67f365035f1f7987c1aefb1fb19dd19d972c299eea28d95be8633589b2d0bbfd1b621c8a886e894ed9a40846d314a43a4503f9  py3-pikepdf-7.1.2.tar.gz
"
