# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=py3-openssl
_pkgname=pyOpenSSL
pkgver=23.1.0
pkgrel=0
pkgdesc="Python3 wrapper module around the OpenSSL library"
url="https://github.com/pyca/pyopenssl"
arch="noarch"
license="Apache-2.0"
depends="python3 py3-cryptography"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="py3-flaky py3-pytest-xdist py3-pretend"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-openssl" # Backwards compatibility
provides="py-openssl=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest -n auto
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
b46b9a4de7ca584f5711089f1a056c479e65e06b9a0868263aa51b3f724d81342cc89fe06c9eb6e8d44fb87e93c7face9643577145d57608ef6162a5be18127b  pyOpenSSL-23.1.0.tar.gz
"
